using System;
using System.Net.NetworkInformation;
using FluentAssertions;
using FluentAssertions.Execution;
using FluentAssertions.Extensions;
using Xunit;

namespace FluentAssertionTraining
{
    public class UnitTest1
    {
        [Fact]
        public void Investigation_String_Content_Success_Test()
        {
            // Arrange
            const string someText = "EFCODEFIRST";

            // Act
            var subString = someText.Substring(0, 2);

            // Assert
            subString.Should().StartWith("EF").And.EndWith("EF").And.Contain("EF").And.HaveLength(2);
        }
        
        [Fact]
        public void Investigation_String_Content_Match_Success_Test()
        {
            // Arrange
            const string someText = "EFCODEFIRST";

            // Act
            var subString = someText.Substring(0, 2);

            // Assert
            subString.Should().Match(s => s.Substring(0, 2) == "EF");
            subString.Should().Match(s => s.Substring(0, 2) == "EF", "Because it is necessary for me");
            subString.Should().NotBe("EC").And.BeOneOf(new[] {"EF", "CF", "FF"} ).And.ContainAll(new []{ "EF", "EF"});
            subString.Should().NotBeEmpty();
        }

        [Fact]
        public void ThrowingExceptionValidation()
        {
            // Arrange
            var messageCollection = new MessageCollection();
            var exceptionMessage = messageCollection.ArgumentNullException;
            var classForCheckThrowingException = new ClassForCheckThrowingException(messageCollection);

            // Act
            Action action = () => classForCheckThrowingException.ThrowArgumentNullException();

            // Assert
            action.Should().Throw<ArgumentNullException>()
                .WithMessage(exceptionMessage);
        }

        [Fact]
        public void Check_CustomerClass()
        {
            // Arrange
            
            var customer1 = new Customer { CustomerId = 5, CustomerName = "Customer1" };
            var customerDto1 = new CustomerDto { CustomerId = 5, CustomerName = "Customer1" };
            var customer2 = new Customer() { CustomerId = 3, CustomerName = "Customer5" };
            string nullValue = null;
            short? theShort = null;
            float theFloat = 3.1456F;
            int theInt32 = 5;
            DateTime theDateTime = new DateTime(2020, 3, 25, 23,55,16 );
            DateTime theDateTimeBeforeTwentyMinutes = new DateTime( 2020, 3, 25, 23, 35, 17 );
            
            // Act
            var customers = new Customers(new [] {customer1, customer2});
            var sameCustomers = customers;

            // Assert
            using (new AssertionScope())
            {
                customers.Should().BeOfType<Customers>().Which.Value.Length.Should().Be(2);
                customers.Should().BeOfType<Customers>().Which.Value[0].CustomerId.Should().Be(5);
                nullValue.Should().BeNull();
                customers.Should().NotBeNull().And.BeSameAs(sameCustomers);
                customer1.Should().NotBeSameAs(customer2);
                customer1.Should().BeOfType<Customer>().Which.CustomerId.Should().Be(5);
                customer2.Should().BeOfType<Customer>().Which.CustomerId.Should().NotBe(5);
                customer1.Should().NotBeNull().And.BeOfType<Customer>().Which.CustomerId.Should();
                theShort.Should().NotHaveValue();
                theShort.Should().BeNull();
                theShort.Should().Match(x => !x.HasValue || x > 0);
                theFloat.Should().BeApproximately(3.14F, 0.01F);
                theFloat.Should().BeLessThan(4L);
                theFloat.Should().BeGreaterThan(3L);
                theFloat.Should().BeInRange(3L, 4L);
                theFloat.Should().BePositive();
                theInt32.Should().BeInRange(3,6);
                theInt32.Should().BeOneOf(new []{ 3, 4, 5 });
                theDateTime.Should().Be(25.March(2020).At(23, 55, 16));
                theDateTime.Should().BeAfter(20.March(2020).At(13, 55));
                theDateTime.Should().BeBefore(26.March(2020).At(13, 55));
                theDateTime.Should().BeOnOrAfter(25.March(2020));
                theDateTime.Should().BeOnOrBefore(26.March(2020));
                theDateTime.Should().BeSameDateAs(25.March(2020));
                theDateTime.Should().BeOneOf(
                    23.March(2020),
                    24.March(2020),
                    25.March(2020).At(23, 55, 16),
                    26.March(2020),
                    27.March(2020),
                    28.March(2020),
                    29.March(2020),
                    30.March(2020),
                    1.April(2020),
                    2.April(2020),
                    3.April(2020),
                    4.April(2020),
                    5.April(2020),
                    6.April(2020)
                );
                theDateTime.Should().HaveDay(25).And.HaveMonth(3).And.HaveHour(23).And.HaveMinute(55);
                theDateTimeBeforeTwentyMinutes.Should().BeLessThan(20.Minutes()).Before(theDateTime);
                theDateTimeBeforeTwentyMinutes.AddSeconds(-1).Should().BeWithin(20.Minutes()).Before(theDateTime);
                theDateTimeBeforeTwentyMinutes.AddSeconds(-3).Should().BeAtLeast(20.Minutes()).Before(theDateTime);
                theDateTimeBeforeTwentyMinutes.AddSeconds(-1).Should().BeExactly(20.Minutes()).Before(theDateTime);
                customer1.Should().BeEquivalentTo(customerDto1);
            }
        }

        private class ClassForCheckThrowingException
        {
            private readonly IMessageCollection _messageCollection;
            
            public ClassForCheckThrowingException(IMessageCollection messageCollection)
            {
                _messageCollection = messageCollection;
            }
            
            internal void ThrowArgumentNullException()
            {
                throw new ArgumentNullException(null, _messageCollection.ArgumentNullException);
            }
        }

        private class MessageCollection: IMessageCollection
        {
            private const string ArgumentNullExceptionMessage = "Some message for argument null exception throwing.";
            public string ArgumentNullException { get; }

            internal MessageCollection()
            {
                ArgumentNullException = ArgumentNullExceptionMessage;
            }
        }
        
        private interface IMessageCollection
        {
            string ArgumentNullException { get; }
        }

        private class Customer
        {
            public int CustomerId { get; set; }

            public string CustomerName { get; set; }
        }

        private class CustomerDto
        {
            public int CustomerId { get; set; }
            
            public string CustomerName { get; set; }
        }

        private class Customers
        {
            public Customers(Customer[] customers)
            {
                Value = customers;
            }
            
            public Customer[] Value { get; set; }
        }
    }
}
